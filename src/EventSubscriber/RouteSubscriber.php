<?php

namespace Drupal\vpn_login\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Implements event subscriber for handling Routes events.
 */
class RouteSubscriber implements EventSubscriberInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new RouteSubscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Listens to the routing event.
   */
  public static function getSubscribedEvents() {
    return [RoutingEvents::ALTER => ['onRouteAlter', -100]];
  }

  /**
   * This method is called whenever the routing table is altered.
   */
  public function onRouteAlter(RouteBuildEvent $event) {
    $config = $this->configFactory->get('ip_login_block.settings');

    // Check if the caching should be disabled.
    if ($config->get('enable_vpn_login')) {
      // Fetch the collection of routes.
      $collection = $event->getRouteCollection();
      // Check if the user.login route is in the collection.
      if ($route = $collection->get('user.login')) {
        // Disable caching for this route.
        $route->setOption('no_cache', TRUE);
      }
    }
  }

}
