<?php

namespace Drupal\vpn_login\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Implements event subscriber for handling login events.
 */
class LoginSubscriber implements EventSubscriberInterface {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new LoginSubscriber.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(RouteMatchInterface $route_match, ConfigFactoryInterface $config_factory) {
    $this->routeMatch = $route_match;
    $this->configFactory = $config_factory;
  }

  /**
   * GetSubscribedEvents.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkAccess', 20];
    return $events;
  }

  /**
   * CheckAccess.
   */
  public function checkAccess(RequestEvent $event) {
    $route_name = $this->routeMatch->getRouteName();

    // Check if the current route is the user login page.
    if ($route_name === 'user.login') {
      $config = $this->configFactory->get('ip_login_block.settings');
      if ($config->get('enable_vpn_login')) {
        $request = $event->getRequest();
        $ip_reference_method = $config->get('ip_reference_method');
        $ips = $this->getIpFromRequest($request, $ip_reference_method);
        $allow_ips = $config->get('allow_ips');
        $response_type = $config->get('response_type');

        $block = TRUE;
        foreach ($ips as $ip) {
          if (IpUtils::checkIp($ip, $allow_ips)) {
            $block = FALSE;
            break;
          }
        }

        if ($block) {
          if ($response_type === '404') {
            throw new NotFoundHttpException();
          }
          else {
            throw new AccessDeniedHttpException();
          }
        }
      }
    }
  }

  /**
   * Gets the IP address from the request based on the selected method.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request.
   * @param string $method
   *   The method to use for getting the IP ('direct_ip' or 'x_forwarded_for').
   *
   * @return string
   *   The IP address.
   */
  protected function getIpFromRequest($request, $method) {
    if ($method === 'x_forwarded_for' && $request->headers->has('X-Forwarded-For')) {
      $ips = explode(',', $request->headers->get('X-Forwarded-For'));

      foreach ($ips as $key => $ip) {
        $parts = explode(":", $ip);
        $ips[$key] = $parts[0];
      }

      return $ips;
    }
    else {
      return [$request->getClientIp()];
    }
  }

}
