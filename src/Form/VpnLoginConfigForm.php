<?php

namespace Drupal\vpn_login\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements config form for module settings.
 */
class VpnLoginConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vpn_login.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vpn_login_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vpn_login.settings');

    $form['enable_vpn_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable VPN login'),
      '#default_value' => $config->get('enable_vpn_login'),
      '#description' => $this->t('Check this box to enable IP blocking on the /user/login page.'),
    ];

    $form['ip_reference_method'] = [
      '#type' => 'select',
      '#title' => $this->t('IP Reference Method'),
      '#options' => [
        'direct_ip' => $this->t('Direct User IP Address'),
        'x_forwarded_for' => $this->t('X-Forwarded-For Header'),
      ],
      '#default_value' => $config->get('ip_reference_method'),
      '#description' => $this->t('Select whether to use the direct user IP or the IP from the X-Forwarded-For header.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_vpn_login"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['allow_ips'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed IP Addresses'),
      '#default_value' => $config->get('allow_ips'),
      '#description' => $this->t('Enter the IP addresses that should have access to the user login page, separated by commas. Supports IP ranges.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_vpn_login"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['response_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Response Type for Blocked IPs'),
      '#options' => [
        '403' => $this->t('403 Forbidden'),
        '404' => $this->t('404 Not Found'),
      ],
      '#default_value' => $config->get('response_type'),
      '#states' => [
        'visible' => [
          ':input[name="enable_vpn_login"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Display the current IP address as a system message.
    $current_ip = $this->getUserIp($form_state);
    $this->messenger()->addMessage($this->t('Your current IP address (based on @method): @ip', [
      '@ip' => $current_ip,
      '@method' => $config->get('ip_reference_method') ? $config->get('ip_reference_method') : 'direct_ip',
    ]));

    return parent::buildForm($form, $form_state);
  }

  /**
   * Helper function to get the user IP based on the current selection.
   */
  protected function getUserIp(FormStateInterface $form_state) {
    $config = $this->config('vpn_login.settings');
    $method = $config->get('ip_reference_method') ? $config->get('ip_reference_method') : 'direct_ip';
    $request = $this->getRequest();
    if ($method === 'x_forwarded_for' && $request->headers->has('X-Forwarded-For')) {
      return $request->headers->get('X-Forwarded-For');
    }
    else {
      return $request->getClientIp();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('vpn_login.settings')
      ->set('enable_vpn_login', $form_state->getValue('enable_vpn_login'))
      ->set('ip_reference_method', $form_state->getValue('ip_reference_method'))
      ->set('allow_ips', $form_state->getValue('allow_ips'))
      ->set('response_type', $form_state->getValue('response_type'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
